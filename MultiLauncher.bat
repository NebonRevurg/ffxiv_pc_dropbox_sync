@echo off
::Determine Directory Structure
echo ---------------- LOCATING GAME FILE DIRECTORY ----------------- %time%
if exist "f:%homepath%\Documents\My Games\FINAL FANTASY XIV - A Realm Reborn" SET XIVPATH="f:%homepath%\Documents\My Games\FINAL FANTASY XIV - A Realm Reborn"
if exist "e:%homepath%\Documents\My Games\FINAL FANTASY XIV - A Realm Reborn" SET XIVPATH="e:%homepath%\Documents\My Games\FINAL FANTASY XIV - A Realm Reborn"
if exist "d:%homepath%\Documents\My Games\FINAL FANTASY XIV - A Realm Reborn" SET XIVPATH="d:%homepath%\Documents\My Games\FINAL FANTASY XIV - A Realm Reborn"
if exist "C:%homepath%\Documents\My Games\FINAL FANTASY XIV - A Realm Reborn" SET XIVPATH="C:%homepath%\Documents\My Games\FINAL FANTASY XIV - A Realm Reborn"
::if exist "f:\Users\%username%\Dropbox\" SET DROPBOX="f:\Users\%username%\Dropbox\FFXIV UI"
::if exist "e:\Users\%username%\Dropbox\" SET DROPBOX="e:\Users\%username%\Dropbox\FFXIV UI"
::if exist "d:\Users\%username%\Dropbox\" SET DROPBOX="d:\Users\%username%\Dropbox\FFXIV UI"
::if exist "C:\Users\%username%\Dropbox\" SET DROPBOX="C:\Users\%username%\Dropbox\FFXIV UI"
if exist "C:%homepath%\Dropbox\" SET DROPBOX="C:%homepath%\Dropbox\FFXIV UI"
if exist "d:%homepath%\Dropbox\" SET DROPBOX="D:%homepath%\Dropbox\FFXIV UI"
if exist "e:%homepath%\Dropbox\" SET DROPBOX="E:%homepath%\Dropbox\FFXIV UI"
if exist "f:%homepath%\Dropbox\" SET DROPBOX="f:%homepath%\Dropbox\FFXIV UI"
if exist "f:%homepath%\Dropbox\" SET DRIVELETTER=F
if exist "e:%homepath%\Dropbox\" SET DRIVELETTER=E
if exist "d:%homepath%\Dropbox\" SET DRIVELETTER=D
if exist "C:%homepath%\Dropbox\" SET DRIVELETTER=C
echo Dropbox Path: %DROPBOX%
echo Final Fantasy XIV Game File Path: %XIVPATH%

echo ----------------- RESTORING FROM BACKUP ----------------------- %time%
:: Next, Download from Dropbox
%DRIVELETTER%:
cd %DROPBOX%
Xcopy %DROPBOX% %XIVPATH% /D /S /Y /EXCLUDE:xcpxclde.txt

echo --------------------- STARTING LAUNCHER ----------------------- %time%
:: Next, Start the Launcher
if exist "C:\Program Files (x86)\SquareEnix\FINAL FANTASY XIV - A Realm Reborn\boot\ffxivboot.exe" start /WAIT "" "C:\Program Files (x86)\SquareEnix\FINAL FANTASY XIV - A Realm Reborn\boot\ffxivboot.exe"
if exist "D:\Program Files (x86)\SquareEnix\FINAL FANTASY XIV - A Realm Reborn\boot\ffxivboot.exe" start /WAIT "" "D:\Program Files (x86)\SquareEnix\FINAL FANTASY XIV - A Realm Reborn\boot\ffxivboot.exe"
if exist "E:\Program Files (x86)\SquareEnix\FINAL FANTASY XIV - A Realm Reborn\boot\ffxivboot.exe" start /WAIT "" "E:\Program Files (x86)\SquareEnix\FINAL FANTASY XIV - A Realm Reborn\boot\ffxivboot.exe"
if exist "F:\Program Files (x86)\SquareEnix\FINAL FANTASY XIV - A Realm Reborn\boot\ffxivboot.exe" start /WAIT "" "F:\Program Files (x86)\SquareEnix\FINAL FANTASY XIV - A Realm Reborn\boot\ffxivboot.exe"

echo ------------------ WAITING FOR GAME TO END -------------------- %time%
SET IMAGENAME_BOOT=ffxivboot.exe
SET IMAGENAME_LAUNCHER=ffxivlauncher.exe
SET IMAGENAME_GAME=ffxiv.exe
SET IMAGENAME_GAME_DX11=ffxiv_dx11.exe

:_WaitLoop
Ping 1.0.0.0 -n 1 -w 10000>Nul
tasklist /NH /FI "IMAGENAME eq %IMAGENAME_BOOT%" 2>Nul|Find /I "%IMAGENAME_BOOT%">Nul
::echo %ERRORLEVEL%
If %ERRORLEVEL%==0 Goto _WaitLoop
tasklist /NH /FI "IMAGENAME eq %IMAGENAME_LAUNCHER%" 2>Nul|Find /I "%IMAGENAME_LAUNCHER%">Nul
::echo %ERRORLEVEL%
If %ERRORLEVEL%==0 Goto _WaitLoop
tasklist /NH /FI "IMAGENAME eq %IMAGENAME_GAME%" 2>Nul|Find /I "%IMAGENAME_GAME%">Nul
::echo %ERRORLEVEL%
If %ERRORLEVEL%==0 Goto _WaitLoop
tasklist /NH /FI "IMAGENAME eq %IMAGENAME_GAME_DX11%" 2>Nul|Find /I "%IMAGENAME_GAME_DX11%">Nul
::echo %ERRORLEVEL%
If %ERRORLEVEL%==0 Goto _WaitLoop
:_Continue

echo --------------------- BACKING UP CONFIGS ---------------------- %time%
:: After Playing, backup the Configuration files back to dropbox
Xcopy %XIVPATH% %DROPBOX% /D /S /Y /EXCLUDE:xcpxclde.txt
%DRIVELETTER%:
cd %DROPBOX%
if exist .\downloads RMDIR .\downloads /S /Q
if exist .\FFXIV.cfg DEL .\FFXIV.cfg
if exist .\FFXIV_BOOT.cfg DEL .\FFXIV_BOOT.cfg
if exist .\TempInstaller RMDIR .\TempInstaller /S /Q

echo -------------------- KILLING CONTROL MAPPER ------------------- %time%

tasklist /NH /FI "IMAGENAME eq ScpServer.exe" 2>Nul|Find /I "ScpServer.exe">Nul
If %ERRORLEVEL%==0 TASKKILL /IM ScpServer.exe

tasklist /NH /FI "IMAGENAME eq ScpServer.exe" 2>Nul|Find /I "ScpServer.exe">Nul
If %ERRORLEVEL%==0 TASKKILL /F /IM ScpServer.exe